package com.musalasoft.dronechallenge.exception;

public class WeightLimitExceedingException  extends RuntimeException{
    public WeightLimitExceedingException(String message){
        super(message);
    }
}
