package com.musalasoft.dronechallenge.exception;

public class LowBatteryException extends RuntimeException {
    public LowBatteryException(String message){
        super(message);
    }
}
