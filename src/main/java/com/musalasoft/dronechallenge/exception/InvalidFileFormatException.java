package com.musalasoft.dronechallenge.exception;

public class InvalidFileFormatException extends RuntimeException{
    public InvalidFileFormatException(String message){
        super(message);
    }
}
