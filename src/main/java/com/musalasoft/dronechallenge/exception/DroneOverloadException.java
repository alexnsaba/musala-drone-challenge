package com.musalasoft.dronechallenge.exception;

public class DroneOverloadException extends RuntimeException {
    public DroneOverloadException(String message){
        super(message);
    }
}
