package com.musalasoft.dronechallenge.dto;

import com.musalasoft.dronechallenge.entity.enums.DroneModel;
import com.musalasoft.dronechallenge.entity.enums.DroneState;
import lombok.Data;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Data
public class DroneDTO {
    private Integer id;

    @Length(min = 2, max = 100, message = "Serial number must have a minimum  5 and a maximum of 100 characters")
    @NotBlank(message = "Serial number can not be empty")
    private String serialNumber;

    @Length(max = 15, message = "drone model should not exceed 15 characters")
    @NotBlank(message = "model can not be empty")
    private String droneModel;

    private DroneModel model;

    @NotNull(message = "weight limit can not be empty")
    private Integer weightLimit;

    @NotNull(message = "battery capacity can not be empty")
    private Double batteryCapacity;

    private DroneState state;
}
