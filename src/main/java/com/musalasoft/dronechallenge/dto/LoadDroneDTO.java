package com.musalasoft.dronechallenge.dto;

import lombok.Data;

import javax.validation.constraints.NotNull;

@Data
public class LoadDroneDTO {
    @NotNull(message = "drone id can not be empty")
    private Integer droneId;

    @NotNull(message = "medication id can not be empty")
    private Integer medicationId;
}
