package com.musalasoft.dronechallenge.repository;

import com.musalasoft.dronechallenge.entity.DroneBatteryLevelAuditLog;
import org.springframework.data.jpa.repository.JpaRepository;

public interface DroneBatteryLevelAuditLogRepository extends JpaRepository<DroneBatteryLevelAuditLog , Integer> {
}
