package com.musalasoft.dronechallenge.repository;

import com.musalasoft.dronechallenge.entity.Drone;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface DroneRepository extends JpaRepository<Drone , Integer> {
    @Query(value =  "SELECT * FROM drones WHERE state = 'IDLE' " , nativeQuery = true)
    List<Drone> checkAvailableDrones();
}
