package com.musalasoft.dronechallenge.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

@Table(name = "medications")
@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Medication {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column(nullable = false)
    @Pattern(regexp ="^[A-Za-z0-9_-]*$", message = "name must contain only letters, numbers,underscore, and dashes")
    @NotBlank(message = "name can not be empty")
    private String name;

    @Column(nullable = false)
    @NotNull(message = "weight can not be empty")
    private int weight;

    @Column(nullable = false)
    @Pattern(regexp ="^[A-Z0-9_]*$", message = "code must contain only upper case letters, numbers, and underscore")
    @NotBlank(message = "code can not be empty")
    private String code;

    @Column(nullable = false)
    private String imageUrl;
}
