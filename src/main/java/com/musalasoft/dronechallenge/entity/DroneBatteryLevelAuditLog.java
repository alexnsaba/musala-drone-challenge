package com.musalasoft.dronechallenge.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDateTime;

@Table(name = "drone_battery_level_audit_logs")
@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
public class DroneBatteryLevelAuditLog {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    private int droneId;

    private double batteryLevel;

    private LocalDateTime createdAt;
}
