package com.musalasoft.dronechallenge.entity;

import com.musalasoft.dronechallenge.entity.enums.DroneModel;
import com.musalasoft.dronechallenge.entity.enums.DroneState;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Table(name = "drones")
@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Drone {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column(nullable = false)
    private String serialNumber;

    @Column(nullable = false)
    @Enumerated(EnumType.STRING)
    private DroneModel model;

    @Column(nullable = false)
    private int weightLimit;

    @Column(nullable = false)
    private double batteryCapacity;

    @Column(nullable = false)
    @Enumerated(EnumType.STRING)
    private DroneState state;

    @ManyToOne
    @JoinColumn(name = "medication_id")
    private Medication medication;
}
