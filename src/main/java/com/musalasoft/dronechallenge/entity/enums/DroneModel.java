package com.musalasoft.dronechallenge.entity.enums;

public enum DroneModel{
    Lightweight, Middleweight, Cruiserweight, Heavyweight
}
