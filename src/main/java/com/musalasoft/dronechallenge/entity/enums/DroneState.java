package com.musalasoft.dronechallenge.entity.enums;

public enum DroneState {
    IDLE, LOADING, LOADED, DELIVERING, DELIVERED, RETURNING
}
