package com.musalasoft.dronechallenge.entity.enums;

public enum ResponseStatus {
    SUCCESS, FAILED
}
