package com.musalasoft.dronechallenge.config;

import com.musalasoft.dronechallenge.entity.enums.ResponseStatus;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class OperationResult {
    private ResponseStatus status;
    private Object result;
}
