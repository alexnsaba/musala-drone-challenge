package com.musalasoft.dronechallenge.config;

import com.musalasoft.dronechallenge.entity.enums.ResponseStatus;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class FailureResult{
    private ResponseStatus status;
    private String errorName;
    private LocalDateTime timestamp;
    private Object message;
}
