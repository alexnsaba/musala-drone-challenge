package com.musalasoft.dronechallenge.config;

import lombok.extern.slf4j.Slf4j;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.annotation.PostConstruct;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

@Configuration
@Slf4j
public class Utility {
    @Value("${image.upload.folder}")
    String imageUploadDirectoryPath;

    //Create image-uploads folder where the uploaded medication images will be stored
    @PostConstruct
    public void createImageUploadFolder() throws IOException {
        if (!new File(imageUploadDirectoryPath).exists()) {
            log.info("......creating image-uploads folder......");
            try {
                Files.createDirectories(Paths.get(imageUploadDirectoryPath));
            } catch (IOException ex) {
                throw new IOException("Error " + ex.getMessage() + " has occurred");
            }
        }
    }

    //Create model mapper bean for mapping DTOs to actual entities
    @Bean
    public ModelMapper modelMapperGenerator() {
        ModelMapper modelMapper = new ModelMapper();
        modelMapper.getConfiguration().setAmbiguityIgnored(true);
        modelMapper.getConfiguration().setSkipNullEnabled(true);
        return modelMapper;
    }
}
