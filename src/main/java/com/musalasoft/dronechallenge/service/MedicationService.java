package com.musalasoft.dronechallenge.service;

import com.musalasoft.dronechallenge.config.OperationResult;
import com.musalasoft.dronechallenge.entity.Medication;
import com.musalasoft.dronechallenge.entity.enums.ResponseStatus;
import com.musalasoft.dronechallenge.exception.InvalidFileFormatException;
import com.musalasoft.dronechallenge.exception.RecordNotFoundException;
import com.musalasoft.dronechallenge.repository.MedicationRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Objects;

@Service
@Slf4j
@RequiredArgsConstructor
public class MedicationService {
    @Value("${image.upload.folder}")
    String imageUploadDirectoryPath;

    @Value("${default.image.extension}")
    String defaultImageExtension;

    private final MedicationRepository medicationRepository;

    public OperationResult registerMedication(Medication medication, MultipartFile image) throws IOException {
        log.info("Inside registerMedication method of MedicationService");

        // Rename the image, Copy it into image-uploads folder, and store the URL in the DB
        if (Objects.requireNonNull(image.getContentType()).contains("image/")) {
            //rename the image
            String imageName = System.currentTimeMillis() + "-medication." + defaultImageExtension;
            Path imagePath = Paths.get(imageUploadDirectoryPath + imageName);
            image.transferTo(imagePath);

            medication.setImageUrl(imageName);

        } else {
            throw new InvalidFileFormatException("Unsupported file");
        }

        //Persist the medication into the DB and return the created medication
        Medication savedMedication = medicationRepository.save(medication);

        return new OperationResult(ResponseStatus.SUCCESS, savedMedication);
    }

    public OperationResult findMedicationById(int id) {
        log.info("Inside findMedicationById method of MedicationService");

        Medication retrievedMedication = medicationRepository.findById(id).
                orElseThrow(() -> new RecordNotFoundException("Medication not Found."));

        return new OperationResult(ResponseStatus.SUCCESS, retrievedMedication);
    }
}
