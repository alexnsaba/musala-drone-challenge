package com.musalasoft.dronechallenge.service;

import com.musalasoft.dronechallenge.config.OperationResult;
import com.musalasoft.dronechallenge.dto.DroneDTO;
import com.musalasoft.dronechallenge.dto.LoadDroneDTO;
import com.musalasoft.dronechallenge.entity.Drone;
import com.musalasoft.dronechallenge.entity.Medication;
import com.musalasoft.dronechallenge.entity.enums.DroneModel;
import com.musalasoft.dronechallenge.entity.enums.DroneState;
import com.musalasoft.dronechallenge.entity.enums.ResponseStatus;
import com.musalasoft.dronechallenge.exception.DroneOverloadException;
import com.musalasoft.dronechallenge.exception.LowBatteryException;
import com.musalasoft.dronechallenge.exception.RecordNotFoundException;
import com.musalasoft.dronechallenge.exception.WeightLimitExceedingException;
import com.musalasoft.dronechallenge.repository.DroneRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@Slf4j
@RequiredArgsConstructor
public class DroneService {
    private final DroneRepository droneRepository;

    private final MedicationService medicationService;

    private final ModelMapper modelMapper;

    public OperationResult registerDrone(DroneDTO droneDTO) {
        log.info("Inside registerDrone method of DroneService");

        //check whether drone exceeds max weight limit
        checkWhetherDroneExceedsWeightLimit(droneDTO.getWeightLimit());

        //Get the drone model enum value from the provided string model
        DroneModel model = checkDroneModel(droneDTO.getDroneModel());

        //Set the model and make the drone idle at the creation time
        droneDTO.setModel(model);
        droneDTO.setState(DroneState.IDLE);

        //Map drone DTO to actual drone object
        Drone drone = modelMapper.map(droneDTO, Drone.class);

        //Persist the drone in the DB and return the registered drone.
        Drone registeredDrone = droneRepository.save(drone);

        return new OperationResult(ResponseStatus.SUCCESS, registeredDrone);
    }

    public OperationResult loadDrone(LoadDroneDTO loadDroneDTO) {
        log.info("Inside loadDrone method of DroneService");
        //Check whether drone exists
        Drone retrievedDrone = (Drone) findDroneById(loadDroneDTO.getDroneId()).getResult();

        //Check whether medication exists
        Medication retrievedMedication = (Medication) medicationService.findMedicationById(loadDroneDTO.getMedicationId()).getResult();

        //Prevent drone from overloading
        preventDroneFromOverLoading(retrievedDrone.getWeightLimit(), retrievedMedication.getWeight());

        //Prevent low battery drone from loading
        preventLowBatteryDroneFromLoading(retrievedDrone.getBatteryCapacity());

        //Load drone with the retrieved medication and set its state to LOADED
        retrievedDrone.setMedication(retrievedMedication);
        retrievedDrone.setState(DroneState.LOADED);

        //Update the loaded drone in the DB and return the loaded drone
        Drone loadedDrone = droneRepository.save(retrievedDrone);

        return new OperationResult(ResponseStatus.SUCCESS, loadedDrone);
    }

    public OperationResult checkLoadedMedicationByDroneId(int droneId) {
        log.info("Inside checkLoadedMedicationByDroneId method of DroneService");

        //Check whether drone exists
        Drone retrievedDrone = (Drone) findDroneById(droneId).getResult();

        return retrievedDrone.getMedication() == null ? new OperationResult(ResponseStatus.SUCCESS, "No loaded medication item found.")
                : new OperationResult(ResponseStatus.SUCCESS, retrievedDrone.getMedication());
    }

    public OperationResult checkAvailableDrones() {
        log.info("Inside checkAvailableDrones method of DroneService");

        List<Drone> availableDrones = droneRepository.checkAvailableDrones();

        return new OperationResult(ResponseStatus.SUCCESS, availableDrones);
    }

    public OperationResult checkDroneBatteryLevel(int droneId) {
        log.info("Inside checkDroneBatteryLevel method of DroneService");

        //Check whether drone exists
        Drone retrievedDrone = (Drone) findDroneById(droneId).getResult();

        //Get the battery level from the drone as a percentage
        double batteryLevelPercentage = retrievedDrone.getBatteryCapacity() * 100;

        String result = "The drone's battery level is : " + batteryLevelPercentage + " %";

        return new OperationResult(ResponseStatus.SUCCESS, result);
    }

    public OperationResult findDroneById(int id) {
        log.info("Inside  findDroneById method of DroneService");

        Drone retrievedDrone = droneRepository.findById(id).orElseThrow(() -> new RecordNotFoundException("Drone not Found."));

        return new OperationResult(ResponseStatus.SUCCESS, retrievedDrone);
    }

    private void preventDroneFromOverLoading(int droneWeightLimit, int medicationWeight) {
        if (medicationWeight > droneWeightLimit) {
            throw new DroneOverloadException("Drone can't load more than its weight limit");
        }
    }

    private void preventLowBatteryDroneFromLoading(double batteryLevel) {
        if (batteryLevel < 0.25) {
            throw new LowBatteryException("Low battery. Loading Drone's battery level should be at least 25 %");
        }
    }

    private void checkWhetherDroneExceedsWeightLimit(int weightLimit) {
        if (weightLimit > 500) {
            throw new WeightLimitExceedingException("Maximum weight Limit(500 gr) is exceeded.");
        }
    }

    private DroneModel checkDroneModel(String droneModel) {
        DroneModel model;

        if (droneModel.equalsIgnoreCase("Lightweight")) {
            model = DroneModel.Lightweight;
        } else if (droneModel.equalsIgnoreCase("Middleweight")) {
            model = DroneModel.Middleweight;
        } else if (droneModel.equalsIgnoreCase("Cruiserweight")) {
            model = DroneModel.Cruiserweight;
        } else if (droneModel.equalsIgnoreCase("Heavyweight")) {
            model = DroneModel.Heavyweight;
        } else {
            throw new RecordNotFoundException("Drone model " + droneModel + " doesn't exist.");
        }

        return model;
    }
}
