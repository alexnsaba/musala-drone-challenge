package com.musalasoft.dronechallenge.service;

import com.musalasoft.dronechallenge.entity.Drone;
import com.musalasoft.dronechallenge.entity.DroneBatteryLevelAuditLog;
import com.musalasoft.dronechallenge.repository.DroneBatteryLevelAuditLogRepository;
import com.musalasoft.dronechallenge.repository.DroneRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

@Component
@Slf4j
@RequiredArgsConstructor
public class ScheduledDroneBatteryLevelAudit {
    private final DroneRepository droneRepository;

    private final DroneBatteryLevelAuditLogRepository droneBatteryLevelAuditLogRepository;

    //check drones battery levels and create history/audit log after every hour
    @Scheduled(fixedRate = 3600000)
    public void logCurrentDroneBatteryLevels() {
        log.info("Inside logCurrentDroneBatteryLevels method of ScheduledDroneBatteryLevelAudit");

        //Retrieve all current drones
        List<Drone> retrievedDrones = droneRepository.findAll();

        List<DroneBatteryLevelAuditLog> droneBatteryLevelAuditLogs = new LinkedList<>();

        //For every current drone record create an audit object and add it to droneBatteryLevelAuditLogs list
        retrievedDrones.parallelStream().map(drone -> {
            DroneBatteryLevelAuditLog droneBatteryLevelAuditLog = new DroneBatteryLevelAuditLog();
            droneBatteryLevelAuditLog.setBatteryLevel(drone.getBatteryCapacity());
            droneBatteryLevelAuditLog.setDroneId(drone.getId());
            droneBatteryLevelAuditLog.setCreatedAt(LocalDateTime.now());

            droneBatteryLevelAuditLogs.add(droneBatteryLevelAuditLog);

            return 0;
        }).collect(Collectors.toList());

        //Persist droneBatteryLevelAuditLogs list in the DB
        droneBatteryLevelAuditLogRepository.saveAll(droneBatteryLevelAuditLogs);
    }
}
