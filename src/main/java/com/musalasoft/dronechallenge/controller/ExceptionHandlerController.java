package com.musalasoft.dronechallenge.controller;

import com.musalasoft.dronechallenge.config.FailureResult;
import com.musalasoft.dronechallenge.entity.enums.ResponseStatus;
import com.musalasoft.dronechallenge.exception.InvalidFileFormatException;
import com.musalasoft.dronechallenge.exception.RecordNotFoundException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.FieldError;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestControllerAdvice
public class ExceptionHandlerController {
    @ExceptionHandler(RecordNotFoundException.class)
    public ResponseEntity<FailureResult> handleRecordNotFoundException(RecordNotFoundException ex) {
        FailureResult failureResult = new FailureResult(ResponseStatus.FAILED, ex.getClass().getSimpleName() ,LocalDateTime.now(), ex.getLocalizedMessage());
        return ResponseEntity.status(HttpStatus.NOT_FOUND).body(failureResult);
    }

    @ExceptionHandler(InvalidFileFormatException.class)
    public ResponseEntity<FailureResult> handleInvalidFileFormatException(InvalidFileFormatException ex) {
        FailureResult failureResult = new FailureResult(ResponseStatus.FAILED, ex.getClass().getSimpleName() ,LocalDateTime.now(), ex.getLocalizedMessage());
        return ResponseEntity.status(HttpStatus.FORBIDDEN).body(failureResult);
    }

    @ExceptionHandler(MethodArgumentNotValidException.class)
    public ResponseEntity<FailureResult> handleValidationException(MethodArgumentNotValidException ex) {
        Map<String, String> errors = new HashMap<>();

        List<ObjectError> errorObject = ex.getBindingResult().getAllErrors();

        for (ObjectError error : errorObject) {
            String fieldName = ((FieldError) error).getField();
            String errorMessage = error.getDefaultMessage();
            errors.put(fieldName, errorMessage);
        }

        FailureResult failureResult = new FailureResult(ResponseStatus.FAILED, ex.getClass().getSimpleName() ,LocalDateTime.now(), errors );
        return ResponseEntity.status(HttpStatus.FORBIDDEN).body(failureResult);
    }

    @ExceptionHandler(Exception.class)
    public ResponseEntity<FailureResult> handleUnknownException(Exception ex) {
        FailureResult failureResult = new FailureResult(ResponseStatus.FAILED, ex.getClass().getSimpleName() ,LocalDateTime.now(), ex.getLocalizedMessage());
        return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(failureResult);
    }
}

