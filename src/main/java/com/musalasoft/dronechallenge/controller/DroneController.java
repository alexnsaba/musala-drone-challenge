package com.musalasoft.dronechallenge.controller;

import com.musalasoft.dronechallenge.config.OperationResult;
import com.musalasoft.dronechallenge.dto.DroneDTO;
import com.musalasoft.dronechallenge.dto.LoadDroneDTO;
import com.musalasoft.dronechallenge.service.DroneService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@Slf4j
@RequestMapping("api/v1/drones")
@RequiredArgsConstructor
public class DroneController {
    private final DroneService droneService;

    @PostMapping
    public ResponseEntity<OperationResult> registerDrone(@Valid @RequestBody DroneDTO droneDTO) {
        log.info("Inside registerDrone method of DroneController");
        return ResponseEntity.status(HttpStatus.CREATED).body(droneService.registerDrone(droneDTO));
    }

    @PutMapping
    public ResponseEntity<OperationResult> loadDrone(@Valid @RequestBody LoadDroneDTO loadDroneDTO) {
        log.info("Inside loadDrone method of DroneController");
        return ResponseEntity.status(HttpStatus.OK).body(droneService.loadDrone(loadDroneDTO));
    }

    @GetMapping("/{id}/medication")
    public ResponseEntity<OperationResult> checkLoadedMedicationByDroneId(@PathVariable("id") int droneId) {
        log.info("Inside checkLoadedMedicationByDroneId method of DroneController");
        return ResponseEntity.status(HttpStatus.OK).body(droneService.checkLoadedMedicationByDroneId(droneId));
    }

    @GetMapping
    public ResponseEntity<OperationResult> checkAvailableDrones() {
        log.info("Inside checkAvailableDrones method of DroneController");
        return ResponseEntity.status(HttpStatus.OK).body(droneService.checkAvailableDrones());
    }

    @GetMapping("/{id}/battery")
    public ResponseEntity<OperationResult> checkDroneBatteryLevel(@PathVariable("id") int droneId) {
        log.info("Inside checkDroneBatteryLevel method of DroneController");
        return ResponseEntity.status(HttpStatus.OK).body(droneService.checkDroneBatteryLevel(droneId));
    }

    @GetMapping("/{id}")
    public ResponseEntity<OperationResult> findDroneById(@PathVariable("id") int id) {
        log.info("Inside findDroneById method of DroneController");
        return ResponseEntity.status(HttpStatus.OK).body(droneService.findDroneById(id));
    }
}
