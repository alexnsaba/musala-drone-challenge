package com.musalasoft.dronechallenge.controller;

import com.musalasoft.dronechallenge.config.OperationResult;
import com.musalasoft.dronechallenge.entity.Medication;
import com.musalasoft.dronechallenge.service.MedicationService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.Valid;
import java.io.IOException;

@RestController
@Slf4j
@RequestMapping("api/v1/medications")
@RequiredArgsConstructor
public class MedicationController {
    private final MedicationService medicationService;

    @PostMapping
    public ResponseEntity<OperationResult> registerMedication(@Valid @RequestPart("medication") Medication medication, @RequestPart("image") MultipartFile image) throws IOException {
        log.info("Inside registerMedication method of MedicationController");
        return ResponseEntity.status(HttpStatus.CREATED).body(medicationService.registerMedication(medication , image));
    }

    @GetMapping("/{id}")
    public ResponseEntity<OperationResult> findMedicationById(@PathVariable("id") int id) {
        log.info("Inside findMedicationById method of MedicationController");

        return ResponseEntity.status(HttpStatus.OK).body(medicationService.findMedicationById(id));
    }
}
