# DRONE SERVICE
This drone service is a RESTful API with 5 main web methods or end points, and other 3 web methods. 
It allows clients to communicate with drones.

To build and run this project, Java 17 (and above) and maven must be installed on your machine.

## How to buid this project
From the project root directory, run the command bellow. 

mvn clean install -DskipTests

## How to run the project
After building, just run the following command to start the application.

mvn spring-boot:run

## How to test the API
Now that our drone service RESTful API is up and running. Let's test how it works.

### Successful API Response Body
This is how a response for a successful request looks like:

    {
        "status": "SUCCESS",
        "result": []
    }

### Failed API Response Body
This is how a response for a failed request looks like:

    {
        "status": "FAILED",
        "error_name": "RecordNotFoundException",
        "timestamp": "2022-12-06T07:00:29.0242605",
        "message": "Drone not Found."
    }

**In case you want to use postman, you can just import the collection( drone-challenge.postman_collection.json). It's on the root directory.** 

In case you don't have postman installed, you can test the API using cURL statements as per the guide bellow:

### Registering a drone

    curl --location --request POST 'http://localhost:9000/api/v1/drones' \
    --header 'Content-Type: application/json' \
    --data-raw '{
        "serial_number":"S23UV",
        "drone_model":"Lightweight",
        "weight_limit":400,
        "battery_capacity":0.5
    }'

### Loading a drone with medication items
In order to load a drone we need to know the drone id and the medication id. 

Since we don't have any medication record registered at a momment, Let's first register a medication record. Rememeber to replace the path of the image to point to your local image. You should get the id of the registered medication from the response.

    curl --location --request POST 'http://localhost:9000/api/v1/medications' \
    --form image=@"/C:/drone-challenge/image-uploads/paracetamol.png" \
    --form 'medication=" {
    \"name\":\"Paracetamol\",
    \"weight\":300,
    \"code\":\"MED_50\"
    }";type=application/json'

 Now that we know the medication id, let's get the id of the drone to be loaded by querying for available drones.

    curl --location --request GET 'http://localhost:9000/api/v1/drones'

 Let's now load the drone with the medication.

    curl --location --request PUT 'http://localhost:9000/api/v1/drones' \
    --header 'Content-Type: application/json' \
    --data-raw '{
        "drone_id":3,
        "medication_id":1
    }'


### Checking loaded medication items for a given drone

    curl --location --request GET 'http://localhost:9000/api/v1/drones/3/medication'

### Checking available drones for loading

    curl --location --request GET 'http://localhost:9000/api/v1/drones'

### Checking drone battery level for a given drone

    curl --location --request GET 'http://localhost:9000/api/v1/drones/3/battery'

# WRAP-UP 

We have managed to build, run and test the drone service API. In case of any issues you can reach out to me on my email: **alexnewzniyo@gmail.com**








